import ShopActionTypes from './shop.types'
import { firestore, convertCollectionsSnapshotToMap } from '../../firebase/firebase-utils'

export const fetchCollectionsStart = () => ({
  type: ShopActionTypes.FETCH_COLLECTIONS_START
})

export const fetchCollectionsSuccess = collectionMaps => ({
  type: ShopActionTypes.FETCH_COLLECTIONS_SUCCESS,
  payload: collectionMaps
})

export const fetchCollectionsFailure = err => ({
  type: ShopActionTypes.FETCH_COLLECTIONS_FAILURE,
  payload: err
})

export const fetchCollectionsStartAsync = () => {
  return dispatch => {
    dispatch(fetchCollectionsStart())
    const collectionRef = firestore.collection('collections')
    
    collectionRef.get().then((snapshot => {
      const collection = convertCollectionsSnapshotToMap(snapshot)
      dispatch(fetchCollectionsSuccess(collection))
    })).catch(err => dispatch(fetchCollectionsFailure(err.message)))
  }
}
